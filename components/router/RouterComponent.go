package router

import (
	"bytes"
	"fmt"
	"gitlab.com/gwf/lib"
	"gitlab.com/gwf/lib/components/pagenotfound"
	"html/template"
)

//go:generate gwf gen component RouterComponent
type RouterComponent struct {
}

func New() *RouterComponent {
	return &RouterComponent{}
}

type interceptData struct {
	RoutesTemplateID string
}

func (c *RouterComponent) Intercept(context *lib.RenderContext, rawTempl string) (string, error) {
	templ, err := template.New("intercept").Delims("[[", "]]").Parse(rawTempl)
	if err != nil {
		return "", err
	}
	// Get the id of the component to navigate to
	var cmp *lib.ComponentSpec
	for _, entry := range context.Routes {
		if entry.Regex.MatchString(context.Path) {
			cmp = entry.Spec
			break
		}
	}
	tmplId := pagenotfound.Spec.TemplateID
	if cmp != nil {
		tmplId = cmp.TemplateID
	}

	data := interceptData{RoutesTemplateID: tmplId}

	var b bytes.Buffer
	err = templ.Execute(&b, data)
	if err != nil {
		return "", fmt.Errorf("failed to execute router template: %w", err)
	}
	return b.String(), nil
}
