package pagenotfound

//go:generate gwf gen component PageNotFoundComponent
type PageNotFoundComponent struct {
	Message string
}

func New() *PageNotFoundComponent {
	return &PageNotFoundComponent{Message: "404 page not found"}
}
