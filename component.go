package lib

import (
	"html/template"
)

type ComponentID string

// Will be implemented by the generated code
type Component interface {
	// Parse the component and convert it to html to add to the view.
	Render(ctx *RenderContext) template.HTML
	Init(params ...interface{}) error
}

// Interfaces the user can implement himself.

// An InterceptComponent implements a function called to change the generated template.
// This makes the user capable of changing the template before it gets executed on the actual component.
type InterceptComponent interface {
	Intercept(ctx *RenderContext, templ string) (string, error)
}

type RenderContext struct {
	Path       string
	Components ComponentList
	Routes     Routes
	Wirer      WireFunc
}

type ComponentSpec struct {
	// ComponentID contains the identifier of the template associated with the component.
	// Matches the name of the struct without the "Component".
	TemplateID string
	Template   string
}

type ComponentList []*ComponentSpec

func (list ComponentList) Get(templateId string) (*ComponentSpec, bool) {
	for _, spec := range list {
		if spec.TemplateID == templateId {
			return spec, true
		}
	}
	return nil, false
}

func (list ComponentList) WithoutTemplateName(name string) ComponentList {
	var cp []*ComponentSpec
	for _, spec := range list {
		if spec.TemplateID != name {
			cp = append(cp, spec)
		}
	}
	return cp
}

// WireFunc is the function to access the dependency mechanism provided by wire.
// It creates a new component from the given templateId and parameters to initialize the component with.
// It is expected to exit on error and return a valid component if successful.
type WireFunc func(templateId string, input ...interface{}) Component

// ComponentFunc generates a function to render all components available in the RenderContext to html
func ComponentFunc(ctx *RenderContext) func(templateId string, params ...interface{}) template.HTML {
	return func(templateId string, params ...interface{}) template.HTML {
		comp := ctx.Wirer(templateId, params...)
		return comp.Render(ctx)
	}
}
