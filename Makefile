PKG_LIST := $(shell go list ./... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*Component.go' | grep -v /vendor/ | grep -v _test.go | grep -v .gen.go)

generate:
	for file in $(GO_FILES); do \
	go generate $$file; \
	done