package lib

import (
	"regexp"
)

type Routes []*RouteEntry

type RouteEntry struct {
	Regex *regexp.Regexp
	Spec  *ComponentSpec
}

type RawRouteEntry struct {
	Regex string
	Spec  *ComponentSpec
}

// NewRoutes creates a new Routes object containing the parameter routes and some defaults
func NewRoutes(r []*RawRouteEntry) Routes {
	var res []*RouteEntry
	for _, entry := range r {
		res = append(res, &RouteEntry{
			Regex: regexp.MustCompile(entry.Regex),
			Spec:  entry.Spec,
		})
	}
	return res
}
