package lib

type EntryPoint struct {
	Spec     *ComponentSpec
	Instance Component
}

type Application struct {
	Root       *EntryPoint
	Components ComponentList
	Routes     Routes
}
